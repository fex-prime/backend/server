const register = require("../model/modells")
// const userProfilemodel=require("../model/userProfile.js")
const nodemailer = require("nodemailer")
const otpModalData = require("../model/OTPLOGIN")
const bcrypt = require("bcryptjs")
const constructionData = require("../model/ConstructionPd")
const VegeDataPo = require("../model/VegetableDataPd")
const GroceryData = require('../model/groseryStorey')
const teaData = require("../model/teaSellerPd")
const PDConstruction = require("../model/ConstructionPd");
const userPDData = require("../model/userPdH")
const userBranchData = require("../model/userBranch")
// const groceryStockPd= require('../model/constructionStockDetails')

const getdata = (req, res) => {
    res.json("hellow")
}

const registerUser = async (req, res) => {
    try {

        const { firstName, lastName, email, phoneNumber, userName, password, token, isLoggedin, currentAppVirson, Otp, deviceId, laltitude, longtitude, location,
            imeiNo, lastLogin, isActive, isDeleted, createdBy, createdOne, modifiedBy, ModifiedOn, plateform, locked, lockedDate, unlockBy, unlockDate, invalidAttempt } = req.body;
        console.log(email)
        const matchNum = await register.findOne({ phoneNumber })
        if (matchNum) return res.json({ message: "mobile number is already exists", success: false });

        const matchEmail = await register.findOne({ email })
        if (matchEmail) return res.json({ message: "emailis already exists", success: false });

        const salt = await bcrypt.genSalt(10)
        const hashedPAssword = await bcrypt.hash(password, salt)

        const user = new register({
            firstName: firstName,
            lastName: lastName,
            email: email,
            phoneNumber: phoneNumber,
            userName: userName,
            token: token,
            isLoggedin: isLoggedin,
            currentAppVirson: currentAppVirson,
            Otp: Otp,
            deviceId: deviceId,
            laltitude: laltitude,
            longtitude: longtitude,
            location: location,
            imeiNo: imeiNo,
            lastLogin: lastLogin,
            isActive: isActive,
            isDeleted: isDeleted,
            createdBy: createdBy,
            createdOne: createdOne,
            modifiedBy: modifiedBy,
            ModifiedOn: ModifiedOn,
            plateform: plateform, locked: locked, lockedDate: lockedDate, unlockBy: unlockBy, unlockDate: unlockDate, invalidAttempt: invalidAttempt,
            password: hashedPAssword
        })

        console.log(user)
        await user.save().then(() => {
            res.json({ message: "user successfully registered", success: true })
        })
    } catch (error) {
        console.log(error)
        res.json({ error })
    }
}

const loginUser = async (req, res) => {
    const { email, number, password } = req.body;
    if (email === undefined) {

        const findNum = await register.findOne({ number })
        if (!findNum) return res.json({ message: "invalid number", success: false })

    } else {

        const findEmail = await register.findOne({ email })
        if (!findEmail) return res.json({ message: "invalid email", success: false })

        const isMatch = await bcrypt.compare(password, findEmail.password)
        console.log(isMatch)
        if (!isMatch) return res.json({ message: "invalid password", success: false });


        const token = await findEmail.generateAuthToken(findEmail._id)

        res.send({ message: "login success", success: true, findEmail, token })
    }



}
const forgottenPassword = async (req, res, next) => {
    try {
        const { email } = req.body;
        console.log(email);
        const findEmail = await register.findOne({ email });
        if (!findEmail) return res.json({ message: "invalid email", success: false })
        console.log("a")

        const transporter = nodemailer.createTransport({
            host: "smtp.office365.com",
            port: 587,
            secure: false,
            auth: {
                user: "noreply@talentachievers.com",
                pass: "TArajVi040423$07"
            },
        });
        console.log("b")
        const user = await register.findOne({ email });
        if (!user) return res.status(200).json({ success: false, message: "User not found" });

        // defining mail options...
        const mailOptions = {
            from: "noreply@talentachievers.com",
            to: email,
            subject: "Password reset link",
            // text: `this is upendra singh This link is valid for only 2 minutes http://localhost:5173/reset-password/${user._id},http://127.0.0.1:5173/activatePassword`,
            text: `hii upendra singh,   
        
        Your transiction has been successfully from your account 8000 for ID :85479621547887 & Ref Id :124105300254001425   
        thanks regard's `,
        }
        // sending mail...
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log({ error });
                res.status(200).json({ success: false, message: "Email not sent" });
            } else {
                console.log("Email sent", info.response);
                res.status(200).json({ success: true, message: "Email sent successfully" });
            }
        })
    } catch (error) {
        next(error);
    }
}

const pdUser = async (req, res) => {
    try {
        const { username, address, phoneNumber } = req.body

        const userPd = new userPDData({
            username: username,
            address: address,
            phoneNumber: phoneNumber
        })

        await userPd.save()
        return res.json({
            message: "user Data posted",
            success: true

        })
    } catch (error) {
        console.log(error)
    }
}

const loginOTp = async (req, res) => {
    try {
        const { otp } = req.body
        const otpdata = new otpModalData({
            otp: otp
        })
        await otpdata.save().then(() => {
            res.json({ message: "succes Otp", success: true })
        })

    } catch (error) {
        console.log(error)
    }
}


const LoginValidate = async (req, res) => {

    const { otp } = req.body;
    const verifyData = await otpModalData.findOne({ otp })
    if (!verifyData) return res.json({ message: "otp invalid", success: false })
    return res.json({ message: "success", success: true })
}

const CalculatorConstructionPD = async (req, res) => {
    try {
        const { areaName, buildUpArea, complitionTime, unSkilledLabour, SkilledLabour, wagesUnSkilledLabour, wagesSkilledLabour, contractCostPerSQFeet } = req.body

        const num1 = complitionTime * unSkilledLabour * wagesUnSkilledLabour
        const num2 = SkilledLabour * complitionTime * wagesSkilledLabour

        const num3 = num1 + num2
        // console.log(num3)
        const num7 = num3
        console.log(num7)


        const num4 = buildUpArea * contractCostPerSQFeet
        console.log(num4)
        num5 = (num4 - num7) / complitionTime * 30

        console.log(num5)
        const PDConstruction = constructionData({
            areaName: areaName, buildUpArea: buildUpArea, contractCostPerSQFeet: contractCostPerSQFeet, complitionTime: complitionTime, unSkilledLabour: unSkilledLabour, SkilledLabour: SkilledLabour, wagesUnSkilledLabour: wagesUnSkilledLabour, wagesSkilledLabour: wagesSkilledLabour, totalValue: num3
            , grossIncome: num4, grossMonthlyIncome: num5
        })
        await PDConstruction.save().then(() => {
            res.json({ message: "", success: true, totalValue: num3, grossIncome: num4, grossMonthlyIncome: num5 })
        })

    } catch (error) {
        console.log(error)
    }
}

const getPdDdata = async (req, res) => {
    const getDataConstruction = await PDConstruction.find({});

    return res.json({ success: true, getDataConstruction, message: "hellow" }).status(200);
    // console.log(getDataConstruction)
}

const VegetableDataPost = async (req, res) => {
    try {
        const { vegename, dailyPurchase, purchasePrise, sellingData, latemoringSale } = req.body

        const num1 = (dailyPurchase * 10) / 100

        const num2 = Math.round(num1)
        console.log(num2)

        const num3 = num2;
        console.log(num3)


        const netQuant = dailyPurchase - (num3 + num2)
        console.log(netQuant)


        const purchaseCost = dailyPurchase * purchasePrise
        console.log(purchaseCost)

        const sellingIncome = (netQuant * sellingData) + (latemoringSale * num2)
        console.log(sellingIncome)

        const DailyIncome = sellingIncome - purchaseCost
        console.log(DailyIncome)

        const workingDays = DailyIncome * 25
        console.log(workingDays)
        // console.log(netResul)
        // console.log(xyz)
        const vege = new VegeDataPo({
            vegename: vegename,
            dailyPurchase: dailyPurchase,
            purchasePrise: purchasePrise,
            sellingData: sellingData,
            latemoringSale: latemoringSale
        })

        await vege.save()
        return res.json({ message: "datya save", success: true })

    } catch (error) {
        console.log(error)
    }
}

const teaDataPost = async (req, res) => {
    try {
        const { cityName, noOfCups, noOfDays, Salary, Rent, otherExpens } = req.body

        const grossMarginDay = noOfCups * 2.63

        console.log(grossMarginDay)

        const grossMarginMnth = grossMarginDay * 25
        console.log(grossMarginMnth)

        const otherExpenses = parseInt(Salary) + parseInt(Rent) + parseInt(otherExpens)
        console.log(otherExpenses)

        const netMonthlyProfit = parseInt(grossMarginMnth) - parseInt(otherExpens)
        console.log(grossMarginMnth)
        console.log(otherExpens)

        console.log(netMonthlyProfit)


        const userTeaPd = new teaData({
            cityName: cityName,
            noOfCups: noOfCups,
            noOfDays: noOfDays,
            Salary: Salary,
            Rent: Rent,
            otherExpens: otherExpens
        })

        await userTeaPd.save()
        return res.json({ message: "datat posted", success: true })

    } catch (error) {
        console.log(error)
    }
}

const GroceryPdPost = async (req, res) => {
    try {
        const { firstMnthIncome, secondMnthIncome, thirdMnthIncome, staffSallery, Electricity, otherExpense, itemName, IndustryMargin, salesRatio, grossProfit } = req.body
        // console.log(req.body)    
        const verifiedFirstMnhIncome = firstMnthIncome * 1.5
        // console.log(verifiedFirstMnhIncome)

        const verifiedSecondMnhIncome = secondMnthIncome * 1.5
        // console.log(verifiedSecondMnhIncome)

        const verifiedThirdMnhIncome = thirdMnthIncome * 1.5
        // console.log(verifiedThirdMnhIncome)

        const average = (parseInt(firstMnthIncome) + parseInt(secondMnthIncome) + parseInt(thirdMnthIncome)) / 3
        // console.log(average)

        const average2 = parseInt(verifiedFirstMnhIncome + verifiedSecondMnhIncome + verifiedThirdMnhIncome) / 3
        // console.log(average2)

        const netSell = Math.round(average + average2)
        // console.log(netSell)

        console.log(req.body)
        const groceryPDDataStore = new GroceryData({
            firstMnthIncome: firstMnthIncome,
            secondMnthIncome: secondMnthIncome,
            thirdMnthIncome: thirdMnthIncome,

            staffSallery: staffSallery,
            Electricity: Electricity,
            otherExpense: otherExpense,
            itemName: itemName,
            IndustryMargin: IndustryMargin,
            salesRatio: salesRatio,
            grossProfit: grossProfit
        });

        
        console.log("Hey", GroceryData)
        console.log("Hey", groceryPDDataStore)
        await groceryPDDataStore.save()
        return res.json({ message: "data posted success ", success: true })

    } catch (error) {
        console.log(error)
    }
}

const dairyPdStore = (req, res) => {
    const { firstVisit, sellingPrice, locationPeriodMonth } = req.body
    const dayDuration = firstVisit * 2
    console.log(dayDuration)

    const dailyIncome = dayDuration * sellingPrice
    console.log(dailyIncome)

    const monthlyIncome = (dailyIncome * 30) * locationPeriodMonth / 12
    console.log(monthlyIncome)

    res.json({ message: "hellow", success: true })
}

const deleteFunc = async (req , res)=>{
    try {
        const {id}=req.params
     const deletedData =  await constructionData.findByIdAndDelete({_id:id});
     console.log(deletedData);
     return res.json({success: true, message: "Deletion Successfull"})
    } catch (error) {
        console.log(error)
    }
}

const updateData = async (req ,res)=>{
    try {

        const {id}=req.params
        console.log(req.body)
        const {areaName, buildUpArea, complitionTime, unSkilledLabour, SkilledLabour, wagesUnSkilledLabour, wagesSkilledLabour, contractCostPerSQFeet}=req.body

    
await constructionData.findByIdAndUpdate({_id:id},{$set:{areaName, buildUpArea, complitionTime, unSkilledLabour, SkilledLabour, wagesUnSkilledLabour, wagesSkilledLabour, contractCostPerSQFeet}},{new:true})
          
return res.json({message:"data updated " , success:true})

    } catch (error) {
        console.log(error)
    }
}






module.exports = { getdata, registerUser, loginUser, forgottenPassword, loginOTp, LoginValidate, CalculatorConstructionPD, getPdDdata, VegetableDataPost, teaDataPost, GroceryPdPost, dairyPdStore, pdUser , deleteFunc ,updateData }