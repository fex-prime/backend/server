const express= require('express')
const router = require("./routes/routes")
const cors = require("cors")
const mongoose = require("./database/database")

const app=express();

app.use(express.json())
app.use(cors())
app.use(express.urlencoded({extended: true}));

app.use("/uploads", express.static("uploads"))

app.use("/",router);

app.listen(5000,()=>{
    console.log("this is port 5000")
    })