const express= require("express")
const {getdata,registerUser, loginUser, forgottenPassword, loginOTp, LoginValidate, CalculatorConstructionPD, getPdDdata, VegetableDataPost, teaDataPost, GroceryPdPost, dairyPdStore, pdUser, deleteFunc, updateData, userbranch} = require("../controller/controller")
const multer = require("multer");
const path = require("path");
const userBranchData = require("../model/userBranch");

const router = express.Router()

router.get("/",getdata)
router.route("/register").post(registerUser)
router.post("/login",loginUser)
router.post("/LoginValidate",LoginValidate)
router.post("/loginotp",loginOTp)
// router.post("/userProfile",userP)
router.post("/forgotpassword",forgottenPassword)
router.post("/constructionPd",CalculatorConstructionPD)
router.get("/getpddata",getPdDdata)
router.post("/vegetablepd",VegetableDataPost)
router.post("/teaDataPost",teaDataPost)
router.post("/groceryPost",GroceryPdPost)
router.post("/dairypost",dairyPdStore)
router.post("/userDataPost",pdUser)
router.post("/delete/:id",deleteFunc)
router.post("/update/:id",updateData)
// router.post("/groceryStore", groceryPdStock);

const storage = multer.diskStorage({
    destination: path.join("./uploads"),
    filename: function (req, file, cb) {
        cb(null, "FILE-" + Date.now() + path.extname(file.originalname));
    }
})

const upload = multer({
    storage: storage,
    limits: {fileSize: 1000000000}
}).single("myFile");

const object = async (req, res, next) => {
    try {
        upload(req, res, () => next())
    } catch (error) {
        console.error(error);
    }
}

router.post("/uploadFile", object, async (req, res) => {
    // console.log("hit")
    try {
        const fileName = req.file.filename;
        const filePathURL = req.file.originalname;
const uploaddata =new  userBranchData({
    fileName :fileName,
    fileUrl:filePathURL
})
await uploaddata.save().then(()=>{
    res.json({message:"file save" , success:true})
})


    } catch (error) {
        console.log(error);
    }
});

module.exports=router;