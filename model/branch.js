
const mongoose = require("mongoose")

const branchSchema = new mongoose.Schema({
branch:{
    type:String
},
id:{
    type:String
},
name:{
    type:String
},
description:{
    type:String
},
imageUrl:{
    type:String
},
isActive:{
    type:String
}
})


const branchMasterData = mongoose.model("branchData" , branchSchema)
module.exports=branchMasterData