const mongoose = require("mongoose")

const constructionSchema = new mongoose.Schema({
    areaName:{
        type:String

    },
    buildUpArea:{
        type:String

    },
    // complitiontime
    complitionTime:{
        type:String

    },
    unSkilledLabour:{
        type:String

    },
    SkilledLabour:{
        type:String

    },
    wagesUnSkilledLabour:{
        type:String

    },
    wagesSkilledLabour:{
        type:String

    },
    contractCostPerSQFeet:{
        type:String
    },
    totalValue:{
        type:String

    },
    grossIncome:{
        type:String

    },
    grossMonthlyIncome:{
        type:String

    }
    
})

const constructionData = mongoose.model("ConstructionDataPd",constructionSchema)

module.exports=constructionData;