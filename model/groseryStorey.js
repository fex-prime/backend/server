const mongoose = require("mongoose")

const grocerySchema = new mongoose.Schema({
    firstMnthIncome:{
      type:String  
    },
    secondMnthIncome:{
      type:String  
    },
    thirdMnthIncome:{
      type:String  
    },
    staffSallery:{
      type:String  
    },
    Electricity:{
      type:String  
    },
    otherExpense:{
      type:String  
    },
    itemName:{
      type:String
  }, 
  IndustryMargin:{
      type:String
  }, 
  salesRatio:{
      type:String
  },
   grossProfit:{
      type:String
   }

})

const GroceryData = mongoose.model("grocerryPD",grocerySchema)
module.exports=GroceryData