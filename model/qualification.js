
const mongoose = require("mongoose")

const userBranchSchema = new mongoose.Schema({
userBranch:{
    type:String
},
id:{
    type:String
},
userId:{
    type:String
},
branchId:{
    type:String
},
isDefualt:{
    type:String
},
createdDate:{
    type:String
},
createdBy:{
    type:String
}
})


const userBranchData = mongoose.model("userbranch" , userBranchSchema)
module.exports=userBranchData