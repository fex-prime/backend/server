

const mongoose = require("mongoose")

const userProfileSchema= new mongoose.Schema({
    userProfile:{
        type:String,
        required:true
    },
    userID:{
        type:String,
        required:true
    },
    firstName:{
        type:String,
        required:true
    },
    lastName:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    phoneNumber:{
        type:String,
        required:true
    },
    dateOfBirth:{
        type:String
    },
    photo:{
        type:String
    },
    qualification:{
        type:String
    },
    experience:{
        type:String
    },
    branch:{
        type:String
    },
    designation:{
        type:String
    }
})

const userProfilemodel = mongoose.model("userProfileData",userProfileSchema)

module.exports=userProfilemodel