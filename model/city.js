
City
Id
StateId
CountryId
Name
Description
ImageURL
IsActive


const mongoose = require("mongoose")

const citySchema = new mongoose.Schema({
city:{
    type:String
},
id:{
    type:String
},
stateId:{
    type:String
},
CountryId:{
    type:String
},
name:{
    type:String
},
description:{
    type:String
},
imageUrl:{
    type:String
},
isActive:{
    type:String
}
})


const cityMasterData = mongoose.model("cityData" , citySchema)
module.exports=cityMasterData