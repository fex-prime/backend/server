
const mongoose = require("mongoose")

const otpSchema = new mongoose.Schema({
    digit1:{
    type:String
},
    digit2:{
    type:String
},
    digit3:{
    type:String
},
    digit4:{
    type:String
},
    digit5:{
    type:String
},
    digit6:{
    type:String
}

})


const otpModalData = mongoose.model("otpdata" , otpSchema)
module.exports=otpModalData 