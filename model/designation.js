
Designation
Id
Name
Description
ImageURL
IsActive



const mongoose = require("mongoose")

const designationSchema = new mongoose.Schema({
id:{
    type:String
},
name:{
    type:String
},
Designation:{
    type:String
},
ImageUrl:{
    type:String
},
isActive:{
    type:String
}
})


const designationData = mongoose.model("profileMasterData" , designationSchema)
module.exports=designationData