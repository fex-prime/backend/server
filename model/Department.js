
const mongoose = require("mongoose")

const departmentSchema = new mongoose.Schema({
department:{
    type:String
},
id:{
    type:String
},
name:{
    type:String
},
description:{
    type:String
},
imageUrl:{
    type:String
},
isActive:{
    type:String
}
})


const departmentUSerProfile = mongoose.model("departmentData" , departmentSchema)
module.exports=departmentUSerProfile