
const mongoose = require("mongoose")

const stateSchema = new mongoose.Schema({
state:{
    type:String
},
id:{
    type:String
},
countryId:{
    type:String
},
name:{
    type:String
},
description:{
    type:String
},
imageUrl:{
    type:String
},
isActive:{
    type:String
}
})


const stateMasterData = mongoose.model("stateData" , stateSchema)
module.exports=stateMasterData