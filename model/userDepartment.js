
const mongoose = require("mongoose")

const userDepartmentSchema = new mongoose.Schema({
userDepartment:{
    type:String
},
id:{
    type:String
},
userId:{
    type:String
},
departmentId:{
    type:String
},
isDefault:{
    type:String
},
createdDate:{
    type:String
},
createdBy:{
    type:String
}
})


const userDepartmentDataMain = mongoose.model("userDepartmentData" , userDepartmentSchema)
module.exports=userDepartmentDataMain