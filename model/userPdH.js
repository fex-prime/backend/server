const mongoose = require("mongoose")

const userpdSchema = mongoose.Schema({
    username:{
        type:String,
        required:true
    },
    address:{
        type:String,
        required:true
    },
    phoneNumber:{
        type:String,
        required:true
    }
})

const userPDData = mongoose.model("userDataPD",userpdSchema )

module.exports=userPDData