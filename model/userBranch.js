
const mongoose = require("mongoose")

const userBranchSchema = new mongoose.Schema({
fileName:{
    type:String,
required:true
},

fileUrl:{
    type:String,
    required:true
}
})


const userBranchData = mongoose.model("userbranch" , userBranchSchema)
module.exports=userBranchData