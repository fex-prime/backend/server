const mongoose = require("mongoose")

const vegetableSchema = new mongoose.Schema({
    vegename:{
        type:String
    },
    dailyPurchase:{
        type:String
    },
    purchasePrise:{
        type:String
    },
    sellingData:{
        type:String
    },
    latemoringSale:{
        type:String
    }

})
 

const VegeDataPo= mongoose.model("VegeDataSchema",vegetableSchema)
module.exports=VegeDataPo