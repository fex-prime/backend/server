
Country
Id
Name
Description
ImageURL
IsActive

const mongoose = require("mongoose")

const countrySchema = new mongoose.Schema({
country:{
    type:String
},
id:{
    type:String
},
name:{
    type:String
},
description:{
    type:String
},
imageUrl:{
    type:String
},
isActive:{
    type:String
}
})


const countryMasterData = mongoose.model("countryData" , countrySchema)
module.exports=countryMasterData