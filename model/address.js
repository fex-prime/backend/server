const mongoose = require("mongoose")

const addressSchema = new mongoose.Schema({
address:{
    type:String
},
id:{
    type:String
},
addressTypeId:{
    type:String
},
line1:{
    type:String
},
line2:{
    type:String
},
landMark:{
    type:String
},
cityId:{
    type:String
},
stateId:{
    type:String
},
CountryId:{
    type:String
},
isActive:{
    type:String
}
})


const addessUSerProfile = mongoose.model("addressData" , addressSchema)
module.exports=addessUSerProfile