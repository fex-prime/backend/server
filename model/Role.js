const mongoose =require("mongoose")

const roleData = new mongoose.Schema({
  role:{
        type:String
  } , 
  id:{
        type:String
  } , 
  name:{
        type:String
  } , 
  description:{
        type:String
  } , 
  imageUrl:{
        type:String
  } , 
 IsActive :{
        type:String
  }
})

const roleDataUser = mongoose.model("RoleData" , roleData)
module.exports=roleDataUser